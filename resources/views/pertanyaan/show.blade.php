@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="{{ asset('../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('page-title')
    <h1>Pertanyaan</h1>
@endsection
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Pertanyaan Table</h3>
            </div>
            <div class="card-body">
                <h4>
                    {{ $pertanyaan->judul }}
                </h4>
                <p>
                    {{ $pertanyaan->isi }}
                </p>
            </div>
        </div>
    </div>
@endsection