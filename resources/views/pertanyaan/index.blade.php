@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="{{ asset('../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('page-title')
    <h1>Pertanyaan</h1>
@endsection
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Pertanyaan Table</h3>
                <a href="/pertanyaan/create" class="btn btn-primary float-right">Tambah</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:10px">#</th>
                            <th>Judul</th>
                            <th>Isi</th>
                            <th style="width:40px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($pertanyaan as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->judul}}</td>
                            <td>{{$value->isi}}</td>
                            <td style="display: flex;">
                                <a href="/pertanyaan/{{$value->id}}" class="btn btn-info btn-sm mx-1">Show</a>
                                <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary btn-sm mx-1">Edit</a>
                                <form action="/pertanyaan/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm mx-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center">No data</td>
                        </tr>  
                    @endforelse 
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            {{-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item"><a class="page-link" href="#">«</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
            </div> --}}
        </div>
        <!-- /.card -->
    </div>
@endsection