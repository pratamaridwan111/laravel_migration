@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="{{ asset('../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('page-title')
    <h1>Edit Pertanyaan</h1>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Pertanyaan</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="/pertanyaan/{{ $pertanyaan->id }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Judul</label>
                                <input type="text" class="form-control" name="judul" id="judul"
                                    placeholder="Masukkan Judul" value="{{$pertanyaan->judul}}">
                                @error('judul')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Isi</label>
                                <textarea class="form-control" name="isi" rows="3" placeholder="Masukkan pertanyaan...">{{ $pertanyaan->isi }}</textarea>
                                @error('isi')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>


                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
