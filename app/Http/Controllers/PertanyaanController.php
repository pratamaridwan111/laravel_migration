<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function index()
    {
        // QUERY BUILDER
        // $pertanyaan = DB::table('pertanyaan')->get();

        // ELOQUENT ORM
        $pertanyaan = Pertanyaan::all();

        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        // QUERY BUILDER
        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);

        // ELOQUENT ORM
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save(); // insert into table(values)

        // MASS ASSIGNMENT
        $pertanyaan = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Data berhasil dibuat!');
    }

    public function show($id)
    {
        // QUERY BUILDER
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        // ELOQUENT ORM
        $pertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        // QUERY BUILDER
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        // ELOQUENT ORM
        $pertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);
        
        // QUERY BUILDER
        // $query = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request["judul"],
        //         'isi' => $request["isi"]
        //     ]);

        // ELOQUENT ORM
        $update = Pertanyaan::where('id', $id)->update([
            'judul' => $request["judul"],
            'isi' => $request["isi"]
        ]);

        return redirect('/pertanyaan');
    }

    public function destroy($id)
    {
        // QUERY BUILDER
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();

        // ELOQUENT ORM
        Pertanyaan::destroy($id);

        return redirect('/pertanyaan');
    }
}
